package com.demo.movies.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.movies.entity.Movies;
import com.demo.movies.service.MoviesService;

@RestController
public class MoviesController {
	
	@Autowired
	MoviesService moviesService;
	
	@PutMapping("/updateMovieName")
	public ResponseEntity<String>updateMovieName(@RequestBody Movies movies){
		moviesService.updateMovieName(movies);
		return new ResponseEntity<String>("updated Successfully",HttpStatus.OK);
	}

}
