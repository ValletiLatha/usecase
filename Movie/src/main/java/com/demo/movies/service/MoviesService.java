package com.demo.movies.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.movies.entity.Movies;
import com.demo.movies.repository.MoviesRepository;
import com.google.common.base.Optional;

@Service
public class MoviesService {

	@Autowired
	MoviesRepository moviesRepository;
	
	
	public ResponseEntity<String> updateMovieName(Movies movies) {
		// TODO Auto-generated method stub
		java.util.Optional<Movies>updateMovieName=moviesRepository.findById(movies.getMovieId());
		return new ResponseEntity<String>("updated Successfully",HttpStatus.OK);
	}

}
